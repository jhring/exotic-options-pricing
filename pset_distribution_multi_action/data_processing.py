# David Dewhurst

from __future__ import print_function
import numpy as np 
from scipy import stats 
import matplotlib.pyplot as plt 


def moving_average(array, order):
	"""moving_average(array, order):
	calculates a moving average of array
	to the specified order 

    Args:
        array (1-d array): array of data of which you want the moving average
        order (int): the order of which you want a moving average

    Returns:
        ret: The moving average of the array calculated to the correct order

    """
	ret = np.cumsum(array, dtype=float)
	ret[order:] = ret[order:] - ret[:-order]
	return ret[order - 1:] / order


def historical_volatility(array, num_days=252):
	"""historical volatility(array, num_days=252):
	calculates the historical volatility of a security

	Args:
		array (1-d array): an array of security prices 
		num_days (int, optional): the number of days over which you want to 
		calculuate the historical volatility. Default is 252 (252 trading 
		days in a year) but 360 and 365 are also common

	Returns:
		the historical volatility of the security 

	"""
	log_daily_returns = [np.log(array[i] / array[i-1]) for i in range(1, 
		len(array))]
	return np.sqrt(num_days * np.var(log_daily_returns))


class time_series_viz(object):
	"""time_series_viz(object)
	A time series visualization class that provides limited methods by which
	to visualize multiple time series of similar time scale on the same axis
	
	Methods:
	
		standalone_display
		Displays visualization in a standalone manner without assuming 
		other axes are present. 

		Args:
			labels (array, optional): ['x-axis', 'y-axis']
			save (boolean, optional): True saves the figure, False does not 
			save. Default is not to save the figure and instead show it on 
			screen
			savename (string, optional): string to save the plot as. 
			Should include the file type, e.g., 'picture.png' 

		append_to_plot
		Appends the visualization to an already-created figure

		Args:
			axes (matplotlib.pyplot axes object): a set of axes to which to
			append the plot
			labels (array, optional): ['x-axis', 'y-axis']
	"""

	def __init__(self, arrays):
		self.ts = arrays

	def standalone_display(self, labels=None, save=False, savename=None):
		fig = plt.figure()
		ax = fig.add_subplot(1, 1, 1)
		try:
			[ax.plot(range(self.ts.shape[0]), self.ts[i, :], 
				'k-') for i in range(1, self.ts.shape[-1])]
		except IndexError:
			ax.plot(range(self.ts.shape[0]), self.ts, 
				'k-')

		ax.set_xticks([])

		if labels is not None:
			try:
				ax.set_xlabel(labels[0])
				ax.set_ylabel(labels[1])
			except:
				raise(IOError, 'labels specified incorrectly')

		if save is False:
			plt.show()
		elif save is True:
			assert(type(savename) is str)
			plt.savefig(savename)

	def append_to_plot(self, axes, labels=None):
		try:
			[axes.plot(range(self.ts.shape[0]), self.ts[i, :], 
				'k-') for i in range(1, self.ts.shape[-1])]
		except IndexError:
			axes.plot(range(self.ts.shape[0]), self.ts, 
				'k-')
		
		if labels is not None:
			try:
				axes.set_xlabel(labels[0])
				axes.set_ylabel(labels[1])
			except:
				raise(IOError, 'labels specified incorrectly')


def high_low_strike_prices(array, prop=0.1):
	""" calculates a "high" and "low" strike price for an option 
	on an asset for heuristic use in genetic programming 

	Args: 
		array (np array): array of security price data
		prop (float, optional): proportion up (down) the high (low) price
		should be 

	Returns:
		high (np array): array of high strike prices 
		low (np array): array of low strike prices 
	"""
	high = [(1. + prop) * price for price in array]
	low = [(1. - prop) * price for price in array]
	return np.asarray(high), np.asarray(low) 


def jensen_shannon_divergence(p, q):
	""" calculates the jensen-shannon divergence of two distributions

	Args:
		p (array): probability distribution
		q (array): probability distribution 

	Returns:
		jensen shannon divergence of the distributions 
	"""
	p = np.asarray(p)
	q = np.asarray(q)
	m = 0.5 * (p + q)
	return 0.5 * (stats.entropy(p, m) + stats.entropy(q, m))