function [price_1, price_2] = vanilla_option_price( settle,...
    maturity, rfr, assetPrices, vols, divs, strike, optSpec )
     % VANILLA_OPTION_PRICE
    % Calculates the prices of two European vanilla options on two
    % underlying assetes. 
    % Asset prices are assumed to follow geometric Brownian motion. 
    % ~~~ PARAMETERS ~~~
    % settle: date on which option is to be traded (usually today's date).
    % Given in the format 'Nov-21-2017'
    % maturity: date on which option can be exercised. This is a European
    % option, so option can only be exercised on one specific date. Given in
    % the format 'Nov-21-2018'
    % rfr: risk-free interest rate, typically rate on three-month T-bill. Given
    % in the format rfr = 0.05
    % assetPrices: array of prices of the underlying, [asset1price asset2price]
    % vols: array of volatilities of the underlying, [vol1 vol2]. Given in
    % decimal form, i.e., if volatility of asset1 is 22%, then vol1 = 0.22
    % divs: array of dividends of the underlying, [div1 div2]. Given in decimal
    % form *annualized*, i.e., if dividend of asset1 is 6% annually, then div1
    % = 0.06
    % strike: price of underlying asset at which the option can be exercised.
    % Assumes that prices have been previously set to the same decade.
    % optSpec: either 'call' or 'put'
    % ~~~RETURNS~~~
    % [price_1, price_2]: a  length 2 array of prices. The first is the
    % price of a vanilla option on the first asset, the second is the price
    % of a vanilla option on the second asset. 

    % prices the option
    Time = yearfrac(settle, maturity, 1);
    [call_1, put_1] = blsprice(assetPrices(1), strike, rfr, Time, vols(1),...
        divs(1)); 
    [call_2, put_2] = blsprice(assetPrices(2), strike, rfr, Time, vols(2),...
        divs(2)); 
    
    if strcmp(optSpec, 'call')
        price_1 = call_1;
        price_2 = call_2;
    elseif strcmp(optSpec, 'put')
        price_1 = put_1;
        price_2 = put_2;
    else 
        price_1 = NaN(1);
        price_2 = NaN(1);
    end
end