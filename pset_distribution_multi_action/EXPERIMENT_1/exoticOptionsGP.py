# Colin M. Van Oort

import random

from deap import tools
from deap import algorithms

from collections import defaultdict

def selTournamentAFPO(individuals, k, tournsize):
	selected = []

	for i in range(k):
		contestants = tools.selRandom(individuals, tournsize)
		selected.append(tools.sortLogNondominated(contestants, 1, first_front_only=True)[0])

	return selected


# Used in cxOnePointAFPO, from DEAP gp.py cxOnePoint
__type__ = object
def cxOnePointAFPO(ind1, ind2):
	"""Randomly select in each individual and exchange each subtree with the
	point as root between each individual.

	:param ind1: First tree participating in the crossover.
	:param ind2: Second tree participating in the crossover.
	:returns: A tuple of two trees.
	"""
	if len(ind1) < 2 or len(ind2) < 2:
		# No crossover on single node tree
		return ind1, ind2

	# List all available primitive types in each individual
	types1 = defaultdict(list)
	types2 = defaultdict(list)
	if ind1.root.ret == __type__:
		# Not STGP optimization
		types1[__type__] = range(1, len(ind1))
		types2[__type__] = range(1, len(ind2))
		common_types = [__type__]
	else:
		for idx, node in enumerate(ind1[1:], 1):
			types1[node.ret].append(idx)
		for idx, node in enumerate(ind2[1:], 1):
			types2[node.ret].append(idx)
		common_types = set(types1.keys()).intersection(set(types2.keys()))

	if len(common_types) > 0:
		type_ = random.choice(list(common_types))

		index1 = random.choice(types1[type_])
		index2 = random.choice(types2[type_])

		slice1 = ind1.searchSubtree(index1)
		slice2 = ind2.searchSubtree(index2)
		ind1[slice1], ind2[slice2] = ind2[slice2], ind1[slice1]

	# Set age of crossed individuals to that of the oldest individual
	if ind1.age > ind2.age:
		ind2.age = ind1.age

	else:
		ind1.age = ind2.age

	return ind1, ind2


def afpo(population, toolbox, cxpb, mutpb, ngen, numNewIndividuals=0, stats=None, halloffame=None, verbose=__debug__):
	# The number of new individuals added to the population in each generation must be non-negative
	assert numNewIndividuals >= 0

	# Initialize the logbook
	logbook = tools.Logbook()
	logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

	# Evaluate the individuals with an invalid fitness
	invalid_ind = [ind for ind in population if not ind.fitness.valid]
	fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
	for ind, fit in zip(invalid_ind, fitnesses):
		ind.fitness.values = fit

	# Initialize the hall of fame
	if halloffame is not None:
		halloffame.update(population)

	# Record the pre-evolution population statistics
	record = stats.compile(population) if stats else {}
	logbook.record(gen=0, nevals=len(invalid_ind), **record)
	if verbose:
		print(logbook.stream)

	# The generational loop
	for gen in range(1, ngen + 1):
		# Select the individuals which pass to the next generation
		offspring = toolbox.select(population, len(population)-numNewIndividuals)

		# Vary the individuals in the population
		offspring = algorithms.varAnd(offspring, toolbox, cxpb, mutpb)

		# add new individuals to the population
		offspring += [toolbox.individual() for _ in range(numNewIndividuals)]

		# Evaluate the individuals with an invalid fitness
		invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
		fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
		for ind, fit in zip(invalid_ind, fitnesses):
			ind.fitness.values = fit

		# Update the hall of fame with the generated individuals
		if halloffame is not None:
			halloffame.update(offspring)

		# Replace the current population by the offspring
		population[:] = offspring

		# Append the current generation statistics to the logbook
		record = stats.compile(population) if stats else {}
		logbook.record(gen=gen, nevals=len(invalid_ind), **record)
		if verbose:
			print(logbook.stream)

		# Increase the age of all individuals which pass into the next generation
		for ind in population:
			ind.age += 1

	return population, logbook