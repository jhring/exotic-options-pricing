# David Dewhurst

from __future__ import print_function
import os 
import re 
import cPickle 
import numpy as np 
import matplotlib.pyplot as plt 
import data_processing as dp 
from collections import OrderedDict 
from scipy import stats 


def get_individuals(up):
	directory = os.listdir(up+'/.')
	exprs = []

	for item in directory:
		if re.match('[0-9]{8}\-[0-9]{8}', item):
			with open(up+'/'+item+'/best_indivs_100_runs.pkl', 'rb') as f:
				expr = cPickle.load(f)
				exprs.append(expr[0])
	
	return exprs 


def calculate_distributions(indivs):

	def _split(delimiters, string, maxsplit=0):
	    regexPattern = '|'.join(map(re.escape, delimiters))
	    return re.split(regexPattern, string, maxsplit)

	delimiters = "(", ",", ")", "\s" 
	nodes = []   

	for j in range(len(indivs)):
		nodes.append(_split(delimiters, indivs[j][0]))

	nodes = [list(filter(None, node)) for node in nodes]
	N = sum([sum([1. for j in node]) for node in nodes])
	expr_dict = {}
	for node in nodes:
		for expr in node:
			expr = expr.strip(' ')  # somehow some entries have a prepended \s 
			try:
				expr_dict[expr] += 1
			except:
				expr_dict[expr] = 1

	prob_expr_dict = dict(expr_dict)

	for key in prob_expr_dict:
		prob_expr_dict[key] /= N

	# separate into dict of primitives common between all
	# in this experiment, these primitives were always used 
	# this is a *frequency* distribution
	prims = ['if_up_total','if_up_day','if_up_week','prog2',
	'prog3','prog4','if_holding_call_high','if_holding_call_low',
	'if_holding_put_high','if_holding_put_low','if_u1_up_day',
	'if_u1_up_week','if_u1_up_total','if_u2_up_day','if_u2_up_week',
	'if_u2_up_total']
	common_prims = {key: expr_dict[key] for key in prims}
	M = sum(common_prims.values())

	# this is a *probability* distribution 
	prob_common_prims = dict(common_prims)

	for key in prob_common_prims:
		prob_common_prims[key] /= float(M)

	expr_dict = OrderedDict(sorted(expr_dict.items(), key=lambda t: t[0]))
	prob_expr_dict = OrderedDict(sorted(prob_expr_dict.items(),
	 key=lambda t: t[0]))
	common_prims = OrderedDict(sorted(common_prims.items(),
	 key=lambda t: t[0]))
	prob_common_prims = OrderedDict(sorted(prob_common_prims.items(),
	 key=lambda t: t[0]))

	return expr_dict, prob_expr_dict, common_prims, prob_common_prims 


if __name__ == "__main__":

	exprs_1 = get_individuals('EXPERIMENT_1')
	exprs_2 = get_individuals('EXPERIMENT_2')

	# frequency distributions for each set of expressions 
	freqs_1, probs_1, prims_1, prob_prims_1 = calculate_distributions(exprs_1)
	freqs_2, probs_2, prims_2, prob_prims_2 = calculate_distributions(exprs_2)
	# now calculate Cressie-Read power divergence statistic 
	# stats.power_divergence sets null to have equal probability in each 
	# category by default 
	cr_stat_1, pval_1 = stats.power_divergence(freqs_1.values(),
	 lambda_="cressie-read")
	cr_stat_2, pval_2 = stats.power_divergence(freqs_2.values(),
	 lambda_="cressie-read")
	# now restrict space to just primitive nodes in common 
	cr_term_stat_1, pval_term_1 = stats.power_divergence(prims_1.values(),
	 lambda_="cressie-read")
	cr_term_stat_2, pval_term_2 = stats.power_divergence(prims_2.values(),
	 lambda_="cressie-read")
	
	# now calculate Cressie-Read power divergence statistic on the difference
	# between the two observed primitive distributions 
	# $H_0$ is that prims_2 is distributed as prims_1 
	cr_stat_diff_prims, pval_diff_prims = stats.power_divergence(prims_2.values(),
		f_exp=prims_1.values(), lambda_="cressie-read")

	# now quantify this difference by calculating the JSD of the two dists 
	jsd_prims = dp.jensen_shannon_divergence(prob_prims_1.values(),
	 prob_prims_2.values())
	
	# now plot distributions and then plot rank ordering 
	plt.hold(True)
	_ = np.arange(len(prims_1))
	wo = plt.bar(_, prob_prims_1.values(), align='center', width=0.7,
	 color='k', alpha=0.4)
	w = plt.bar(_, prob_prims_2.values(), align='center', width=0.7,
	 color='r', alpha=0.5)
	#plt.xticks(_, prims_1.keys())
	plt.ylim(0, max(np.maximum(prob_prims_1.values(),
	 prob_prims_2.values())) + 0.01)
	plt.xlim(-0.5,15.5)
	plt.legend((wo[0], w[0]), ('without trading equities',
	 'with trading equities'), loc='best')
	plt.xlabel('primitive operation')
	plt.ylabel('$P($ primitive operation $)$')
	#plt.show()
	plt.savefig('primitive_prob_dists.png')

	RESULTS = [['cr_stat_1, pval_1, cr_stat_2, pval_2, cr_term_stat_1, pval_term_1, cr_term_stat_2, pval_term_2, cr_stat_diff_prims, pval_diff_prims, jsd_prims', 'prims_1.keys()'],
	[cr_stat_1, pval_1, cr_stat_2, pval_2, cr_term_stat_1, pval_term_1, 
	cr_term_stat_2, pval_term_2, cr_stat_diff_prims, pval_diff_prims, 
	jsd_prims, prims_1.keys()]]

	print(RESULTS)

	with open('results.pkl', 'wb') as f:
		cPickle.dump(RESULTS, f)