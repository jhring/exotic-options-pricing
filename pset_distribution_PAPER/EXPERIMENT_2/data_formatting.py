# David Dewhurst

from __future__ import print_function
import csv 
import sys 
import subprocess 
from glob import glob 
from datetime import datetime 

import numpy as np 
import matplotlib.pyplot as plt 
import data_processing as dp 
from scipy.stats import pearsonr 
from bisect import bisect_left, bisect_right 
from collections import OrderedDict 


def get_data(directory):
	""" gets data in the QuantQuote format 

	Args:
		directory (str): directory where the QuantQuote data is stored, 
		e.g., 'daily/'

	Returns:
		securities (dict): dict of np arrays. One dict entry per security, 
		each security has an array for each date. 
		Dates are in order from least recent to most recent. 
	"""
	files = glob(directory+'*.csv')
	securities = {}

	for file in files:
		with open(file, 'rb') as f:
			reader = csv.reader(f)
			securities[file[12:-4]] = []

			for row in reader:
				row[0] = datetime(year=int(row[0][0:4]),
				 month=int(row[0][4:6]), day=int(row[0][6:8]))
				row[1:] = [float(j) for j in row[1:]]
				securities[file[12:-4]].append(np.asarray(row))
			securities[file[12:-4]] = np.asarray(securities[file[12:-4]])

	return securities


def parsedate(date):
		return datetime(year=int(date[0:4]), month=int(date[4:6]), 
			day=int(date[6:8]))


def get_rfr(date_range):
	pass 


def generate_data_4_rainbow(sec1, sec2, date_range, maturity, 
	maxmin, rfrs, when='close', strike_base='max'):
	""" generates data to pass to MATLAB for calculating rainbow option price

	Args:
		sec1 (np array): securities['tckr'], a numpy array in the format 
		[datetime, float, float, float, float, float, float]
		sec2 (np array): securities['tckr'], a numpy array in the format 
		[datetime, float, float, float, float, float, float]
		date_range (array): [date_1, date_2], date_1 is the date to start
		pricing, date_2 is the date to end pricing. 
		MUST BE IN THE FORMAT 'YYYYMMDD'
		maturity (str): the day on which option expires
		MUST BE IN THE FORMAT 'YYYYMMDD'
		maxmin (bool): 1 prices option on maximum of two prices, 0 on
		the minimum of two prices
		rfrs (array): array of risk free interest rates on trading days 
		when(str): 'close' or 'open', when we look at the underlying price 
		strike_base (str, default 'max'): when we calculate possible 
		heuristic strike prices for the options, we need to define which 
		underlying asset is the baseline for "high" and "low" stike prices.
		Default is the asset that initially has a higher price. 
		Can also accept 'min' for the asset that initially has a lower price
	"""

	# get securities for pricer 
	if when != 'close':
		if when == 'open':
			s1 = sec1[:,2]
			s2 = sec2[:,2]
		else:
			raise(ValueError, 'Only makes sense to consider open and close')
	else:
		s1 = sec1[:,5]
		s2 = sec2[:,5]

	# restrict data to date range
	d1 = parsedate(date_range[0])
	d2 = parsedate(date_range[1])
	maturity = parsedate(maturity)

	# do binary search to find start and end dates 
	keys = [date for date in sec1[:,0]]
	left = bisect_left(keys, parsedate(date_range[0]))
	right = bisect_right(keys, parsedate(date_range[1]))
	dates = sec1[left:right, 0]  # dates between start and end inclusive
	# below puts dates in proper formatting for matlab input 
	dates = np.asarray([date.strftime("%b-%d-%Y") for date in dates])

	s1 = s1[left:right]  # asset price 1
	s2 = s2[left:right]  # asset price 2 
	s1_high, s1_low = dp.high_low_strike_prices(s1)  # "high" strike
	s2_high, s2_low = dp.high_low_strike_prices(s2)  # "low" strike 
	vols1 = dp.historical_volatility(s1) * np.ones(s1.shape[0])
	vols2 = dp.historical_volatility(s2) * np.ones(s2.shape[0])
	divs1 = np.zeros(s1.shape[0])  # zero for now, can get real data
	divs2 = np.zeros(s1.shape[0])  # zero for now, can get real data 
	corrs = pearsonr(s1, s2)[0] * np.ones(s1.shape[0])
	varieties = np.asarray(['call', 'put'])
	maturities = np.asarray(
		[maturity.strftime("%b-%d-%Y") for _ in np.ones(s1.shape[0])])
	basis = np.asarray([1 for _ in np.ones(s1.shape[0])])
	# *****
	rfrs = 1. * np.ones(s1.shape[0])  # FIX THIS 
	# *****
	maxmin = maxmin * np.ones(s1.shape[0])

	""" now make it into a big matrix for writing to csv 
	Remember that columns must be in the format 
	settle,maturity,rfr,basis,assetPrice1,assetPrice2,vols1,vols2,divs1,divs2,strike,corrs,optSpec,maxmin
	"""

	# this implements initial price basing of heuristic "high" and "low"
	if strike_base == 'max':
		if s1_high[0] >= s2_high[0]:
			high = s1_high
		else:
			high = s2_high
		if s1_low[0] >= s2_low[0]:
			low = s1_low
		else:
			low = s2_low
	elif strike_base == 'min':
		if s1_high[0] <= s2_high[0]:
			high = s1_high
		else:
			high = s2_high
		if s1_low[0] <= s2_low[0]:
			low = s1_low
		else:
			low = s2_low
	else:
		raise(ValueError, 'Must base heuristic strikes off min or max')

	rainbow_data = []
	file_names = ['r_call_high.csv', 'r_call_low.csv', 
	'r_put_high.csv', 'r_put_low.csv']
	for v in varieties:
		vs = np.asarray([v for _ in np.ones(s1.shape[0])])
		# high strikes 
		#print(dates.shape, maturities.shape, rfrs.shape, basis.shape, 
		#	s1.shape, s2.shape, vols1.shape, vols2.shape, divs1.shape, 
		#	divs2.shape, high.shape, corrs.shape, vs.shape, maxmin.shape)
		#exit()
		rainbow_data.append(np.column_stack(
			(dates, maturities, rfrs, basis, s1, s2, vols1, vols2, 
				divs1, divs2, high, corrs, vs, maxmin)))
		# low strikes 
		rainbow_data.append(np.column_stack(
			(dates, maturities, rfrs, basis, s1, s2, vols1, vols2, 
				divs1, divs2, low, corrs, vs, maxmin)))

	# write to csv files 

	ir = 0
	for name in file_names:
		with open(name, 'wb') as write_file:
			writer = csv.writer(write_file)
			writer.writerow(
				['settle','maturity','rfr','basis','assetPrice1',
				'assetPrice2','vols1','vols2','divs1','divs2',
				'strike','corrs','optSpec','maxmin']
				)
			for row in rainbow_data[ir]:
				writer.writerow(row)
		ir += 1


if __name__ == "__main__":

	""" data is in the format
	date | time | open | high | low | close | volume 
	"""

	DIRECTORY = 'daily/'
	START_DATE = sys.argv[1] #'20040506'
	END_DATE = sys.argv[2] #'20041106'
	date_range = [START_DATE, END_DATE]
	MATURITY =  sys.argv[3] #'20050506'
	SEC1 = sys.argv[4]
	SEC2 = sys.argv[5]
	maxmin = 1
	rfrs = None

	securities = get_data(DIRECTORY)

	# generating data for rainbow option pricer 
	sec1, sec2 = securities[SEC1], securities[SEC2]
	generate_data_4_rainbow(sec1, sec2, date_range, MATURITY, 
		maxmin, rfrs, when='close', strike_base='max')