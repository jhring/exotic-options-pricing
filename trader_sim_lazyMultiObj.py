#John H. Ring IV

import random
import pickle
import operator

from functools import partial
from scoop import futures
import numpy as np
from collections import defaultdict

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

import matplotlib.pyplot as plt
import networkx


def progn(*args):
    for arg in args:
        arg()

def prog2(out1, out2):
    return partial(progn, out1)

def prog3(out1, out2, out3):
    return partial(progn, out1, out2, out3)

def prog4(out1, out2, out3, out4):
    return partial(progn, out1, out2, out3, out4)

def if_then_else(condition, out1, out2):
    out1() if condition() else out2()

class TraderSim(object):
    def __init__(self, cash, fee, ch, cl, ph, pl):
        self.load_data(ch, cl, ph, pl)
        self.start_cash = cash
        self.max_steps = len(self.CH) - 1
        self.steps = 0
        self.cash = cash
        self.fee = fee
        self.holdings = [0, 0, 0, 0]
        self.values = np.zeros(self.max_steps)
        self.values[0] = cash
        self.algo = None

    def _reset(self):
        self.steps = 0
        self.cash = self.start_cash
        self.holdings = [0, 0, 0, 0]
        self.values = np.zeros(self.max_steps+1)
        self.values[0] = self.cash

    def get_call_high(self):
        return self.CH[self.steps]

    def get_call_low(self):
        return self.CL[self.steps]

    def get_put_high(self):
        return self.PH[self.steps]

    def get_put_low(self):
        return self.PL[self.steps]

    def get_value(self):
        return self.cash + \
            self.holdings[0] * self.get_call_high() + \
            self.holdings[1] * self.get_call_low() + \
            self.holdings[2] * self.get_put_high() + \
            self.holdings[3] * self.get_put_low()

    def buy_call_high(self):
        if self.steps < self.max_steps:
            self.steps += 1
            price = self.get_call_high() + self.fee
            if self.cash >= price:
                self.holdings[0] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()

    def buy_call_low(self):
        if self.steps < self.max_steps:
            self.steps += 1;
            price = self.get_call_low() + self.fee
            if self.cash >= price:
                self.holdings[1] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()

    def buy_put_high(self):
        if self.steps < self.max_steps:
            self.steps += 1;
            price = self.get_put_low() + self.fee
            if self.cash >= price:
                self.holdings[2] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()

    def buy_put_low(self):
        if self.steps < self.max_steps:
            self.steps += 1
            price = self.get_put_low() + self.fee
            if self.cash >= price:
                self.holdings[3] += 1
                self.cash -= price
                self.values[self.steps] = self.get_value()

    def sell_call_high(self):
        if self.steps < self.max_steps:
            self.steps += 1
            if self.holdings[0] > 0:
                self.cash += self.get_call_high() - self.fee
                self.holdings[0] -= 1
                self.values[self.steps] = self.get_value()

    def sell_call_low(self):
        if self.steps < self.max_steps:
            self.steps += 1
            if self.holdings[1] > 0:
                self.cash += self.get_call_low() - self.fee
                self.holdings[1] -= 1
                self.values[self.steps] = self.get_value()

    def sell_put_high(self):
        if self.steps < self.max_steps:
            self.steps += 1
            if self.holdings[2] > 0:
                self.cash += self.get_put_high() - self.fee
                self.holdings[2] -= 1
                self.values[self.steps] = self.get_value()

    def sell_put_low(self):
        if self.steps < self.max_steps:
            self.steps += 1
            if self.holdings[3] > 0:
                self.cash += self.get_put_high() - self.fee
                self.holdings[3] -= 1
                self.values[self.steps] = self.get_value()
                
    def do_nothing(self):
        if self.steps < self.max_steps:
            self.steps += 1
            self.values[self.steps] = self.get_value()

    def up_total(self):
        return self.values[self.steps] > self.values[0]

    def up_day(self):
        ret = True
        if self.steps >= 1:
            ret = self.values[self.steps] > self.values[self.steps - 1]
        return ret

    def up_week(self):
        ret = True
        if self.steps >= 5:
            ret = self.values[self.steps] > self.values[self.steps - 5]
        return ret

    def u1_up_total(self):
        return self.U1[self.steps] > self.U1[0]

    def u1_up_day(self):
        ret = True
        if self.steps >= 1:
            ret = self.U1[self.steps] > self.U1[self.steps - 1]
        return ret

    def u1_up_week(self):
        ret = True
        if self.steps >= 5:
            ret = self.U1[self.steps] > self.U1[self.steps - 5]
        return ret

    def u2_up_total(self):
        return self.U2[self.steps] > self.U2[0]

    def u2_up_day(self):
        ret = True
        if self.steps >= 1:
            ret = self.U2[self.steps] > self.U2[self.steps - 1]
        return ret

    def u2_up_week(self):
        ret = True
        if self.steps >= 5:
            ret = self.U2[self.steps] > self.U2[self.steps - 5]
        return ret

    def holding_call_high(self):
        return self.holdings[0] > 0

    def holding_call_low(self):
        return self.holdings[1] > 0

    def holding_put_high(self):
        return self.holdings[2] > 0

    def holding_put_low(self):
        return self.holdings[3] > 0

    def if_up_total(self, out1, out2):
        return partial(if_then_else, self.up_total, out1, out2)

    def if_up_day(self, out1, out2):
        return partial(if_then_else, self.up_day, out1, out2)

    def if_up_week(self, out1, out2):
        return partial(if_then_else, self.up_week, out1, out2)

    def if_u1_up_total(self, out1, out2):
        return partial(if_then_else, self.u1_up_total, out1, out2)

    def if_u1_up_day(self, out1, out2):
        return partial(if_then_else, self.u1_up_day, out1, out2)

    def if_u1_up_week(self, out1, out2):
        return partial(if_then_else, self.u1_up_week, out1, out2)

    def if_u2_up_total(self, out1, out2):
        return partial(if_then_else, self.u2_up_total, out1, out2)

    def if_u2_up_day(self, out1, out2):
        return partial(if_then_else, self.u2_up_day, out1, out2)

    def if_u2_up_week(self, out1, out2):
        return partial(if_then_else, self.u2_up_week, out1, out2)

    def if_holding_call_high(self, out1, out2):
        return partial(if_then_else, self.holding_call_high, out1, out2)

    def if_holding_call_low(self, out1, out2):
        return partial(if_then_else, self.holding_call_low, out1, out2)

    def if_holding_put_high(self, out1, out2):
        return partial(if_then_else, self.holding_put_high, out1, out2)

    def if_holding_put_low(self, out1, out2):
        holding = self.holdings[3] > 0
        return partial(if_then_else, self.holding_put_low, out1, out2)

    def run(self, algo):
        self._reset()
        while self.steps < self.max_steps:
            algo()

    def load_data(self, chStr, clStr, phStr, plStr):
        chFile = open(chStr, "rb")
        clFile = open(clStr, "rb")
        phFile = open(phStr, "rb")
        plFile = open(plStr, "rb")

        ch = pickle.load(chFile)
        cl = pickle.load(clFile)
        ph = pickle.load(phFile)
        pl = pickle.load(plFile)

        self.CH = [float(el[14]) for el in ch[1:]]
        self.CL = [float(el[14]) for el in cl[1:]]
        self.PH = [float(el[14]) for el in ph[1:]]
        self.PL = [float(el[14]) for el in pl[1:]]
        self.U1 = [float(el[4]) for el in pl[1:]]
        self.U2 = [float(el[5]) for el in pl[1:]]

        chFile.close()
        clFile.close()
        phFile.close()
        plFile.close()


# Used in cxOnePointAFPO, from DEAP gp.py cxOnePoint
__type__ = object
def cxOnePointAFPO(ind1, ind2):
    """Randomly select in each individual and exchange each subtree with the
    point as root between each individual.

    :param ind1: First tree participating in the crossover.
    :param ind2: Second tree participating in the crossover.
    :returns: A tuple of two trees.
    """
    if len(ind1) < 2 or len(ind2) < 2:
        # No crossover on single node tree
        return ind1, ind2

    # List all available primitive types in each individual
    types1 = defaultdict(list)
    types2 = defaultdict(list)
    if ind1.root.ret == __type__:
        # Not STGP optimization
        types1[__type__] = range(1, len(ind1))
        types2[__type__] = range(1, len(ind2))
        common_types = [__type__]
    else:
        for idx, node in enumerate(ind1[1:], 1):
            types1[node.ret].append(idx)
        for idx, node in enumerate(ind2[1:], 1):
            types2[node.ret].append(idx)
        common_types = set(types1.keys()).intersection(set(types2.keys()))

    if len(common_types) > 0:
        type_ = random.choice(list(common_types))

        index1 = random.choice(types1[type_])
        index2 = random.choice(types2[type_])

        slice1 = ind1.searchSubtree(index1)
        slice2 = ind2.searchSubtree(index2)
        ind1[slice1], ind2[slice2] = ind2[slice2], ind1[slice1]

    # Set age of crossed individuals to that of the oldest individual
    if ind1.age > ind2.age:
        ind2.age = ind1.age

    else:
        ind1.age = ind2.age

    return ind1, ind2


trader = TraderSim(10000,0, "ch.pkl", "cl.pkl", "ph.pkl", "pl.pkl")
#history = tools.History()

pset = gp.PrimitiveSet("MAIN", 0)
pset.addPrimitive(trader.if_up_total, 2)
pset.addPrimitive(trader.if_up_day, 2)
pset.addPrimitive(trader.if_up_week, 2)
pset.addPrimitive(prog2, 2)
pset.addPrimitive(prog3, 3)
pset.addPrimitive(prog4, 4)
pset.addPrimitive(trader.if_holding_call_high, 2)
pset.addPrimitive(trader.if_holding_call_low, 2)
pset.addPrimitive(trader.if_holding_put_high, 2)
pset.addPrimitive(trader.if_holding_put_low, 2)
pset.addPrimitive(trader.if_u1_up_day, 2)
pset.addPrimitive(trader.if_u1_up_week, 2)
pset.addPrimitive(trader.if_u1_up_total, 2)
pset.addPrimitive(trader.if_u2_up_day, 2)
pset.addPrimitive(trader.if_u2_up_week, 2)
pset.addPrimitive(trader.if_u2_up_total, 2)
pset.addTerminal(trader.buy_call_high)
pset.addTerminal(trader.buy_call_low)
pset.addTerminal(trader.buy_put_low)
pset.addTerminal(trader.buy_put_high)
pset.addTerminal(trader.do_nothing)

creator.create("FitnessMax", base.Fitness, weights=(1.0, -1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMax, pset=pset, age=1)

toolbox = base.Toolbox()

# Enable SCOOP
toolbox.register("map", futures.map)

# Attribute generator
toolbox.register("expr_init", gp.genHalfAndHalf, pset=pset, min_=1, max_=2)

# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr_init)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def evalTrader(individual):
    # Transform the tree expression to functional Python code
    algo = gp.compile(individual, pset)

    trader.run(algo)
    #print(trader.values)
    ret = trader.values[trader.steps] - trader.values[0]

    individual.age += 1

    return (ret, individual.age-1,)


toolbox.register("evaluate", evalTrader)
toolbox.register("select", tools.selDoubleTournament, fitness_size=7, parsimony_size=1,
        fitness_first=True)
#toolbox.register("select", tools.selNSGA2)
toolbox.register("mate", cxOnePointAFPO)
toolbox.register("expr_mut", gp.genHalfAndHalf, min_=0, max_=2)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)
toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=30))
toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=30))
#toolbox.decorate("mate", history.decorator)
#toolbox.decorate("mutate", history.decorator)

def main():
    #random.seed(1)

    pop = toolbox.population(n=100)
    #history.update(pop)
    hof = tools.HallOfFame(1)
    stats_fitness = tools.Statistics(lambda ind: ind.fitness.values[0])
    stats_size = tools.Statistics(lambda ind: ind.height)
    stats_age = tools.Statistics(lambda ind: ind.fitness.values[1])
    mstats = tools.MultiStatistics(fitness=stats_fitness, size=stats_size, age=stats_age)
    mstats.register("avg", np.mean)
    #mstats.register("std", np.std)
    mstats.register("min", np.min)
    mstats.register("max", np.max)
    
    pop, logbook = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.3, 
        ngen=100, stats=mstats, halloffame=hof)

    return pop, logbook, hof

if __name__ == "__main__":

    EXPERIMENT = None
    RUNS = 100

    if EXPERIMENT is None:
        # testing block
        pop, logbook, hof = main()
        print(hof[0])


    elif EXPERIMENT == 1:
        securities = ['aapl', 't']
        """ Here, we'll run a lot of runs on the same underlyings and see
        what type of individuals we get out 
        """
        bests = []
        avg_fitness = []
        std_fitness = []
        avg_size = []
        std_size = []

        for _ in range(RUNS):
            pop, logbook, hof = main()
            bests.append(hof[0])
            avg_fitness.append(logbook.chapters['fitness'].select('mean'))
            std_fitness.append(logbook.chapters['fitness'].select('std'))
            avg_size.append(logbook.chapters['size'].select('mean'))
            std_size.append(logbook.chapters['size'].select('std'))
            

    #graph = networkx.DiGraph(history.genealogy_tree)
    #graph = graph.reverse()     # Make the grah top-down
    #colors = [toolbox.evaluate(history.genealogy_history[i])[0] for i in graph]
    #networkx.draw(graph, node_color=colors)
    #plt.show()