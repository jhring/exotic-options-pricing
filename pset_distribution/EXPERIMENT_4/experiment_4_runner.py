# David Dewhurst

from __future__ import print_function 
import subprocess 
import os 
import shutil 
from itertools import product 


if __name__ == "__main__":

	DATES = [['19980105', '19980506', '19990105'], 
	['19980506', '19981106', '19990506'], 
	['19990105', '19990506', '20000105'], 
	['19990506', '19991106', '20000506'], 
	['20000105', '20000506', '20010105'],
	['20000506', '20001106', '20010506'],
	['20010105', '20010506', '20020105'],
	['20010506', '20011106', '20020506'],
	['20020105', '20020506', '20030105'], 
	['20020506', '20021106', '20030506'], 
	['20030105', '20030506', '20040105'],
	['20030506', '20031106', '20040506']]
	SECURITIES = [['aapl', 't'], ['ibm', 'xom'], ['msft', 'x']]

	for date, sec in product(DATES, SECURITIES):
		path = date[0]+'-'+date[1]+'-'+date[2]+sec[0]+'-'+sec[1]
		if os.path.exists(path):
			shutil.rmtree(path)
		os.makedirs(path)
		subprocess.call('python data_formatting.py '+date[0]+' '+date[1]+' '+date[2]+' '+sec[0]+' '+sec[1],
		 shell=True)
		subprocess.call('bash data_2_matlab.sh', shell=True)
		subprocess.call('python data_2_trader.py', shell=True)
		subprocess.call('python trader_sim.py', shell=True)
		subprocess.call('mv experiment_4_100_runs.png best_indivs_100_runs.pkl '+path,
		 shell=True)