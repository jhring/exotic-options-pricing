from __future__ import print_function
from scoop import futures
import scoop

def helloWorld(value):
    return "Hello World from Future "+ str(value) +". Worker info: " + str(scoop.worker)#".".join([str(scoop.worker[i]) for i in range(len(scoop.worker))])

if __name__ == "__main__":
    returnValues = list(futures.map(helloWorld, range(20)))
    print("\n".join(returnValues))