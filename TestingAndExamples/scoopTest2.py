from math import hypot
from random import random
from scoop import futures

def test(tries):
    return sum(hypot(random(), random()) < 1 for _ in range(tries))

def calcPi(nbFutures, tries):
    expr = futures.map(test, [tries] * nbFutures)
    return 4. * sum(expr) / float(nbFutures * tries)

if __name__ == "__main__":
    print("pi = {}".format(calcPi(100, 500000)))