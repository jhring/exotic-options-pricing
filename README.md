### Evolving trading strategies for exotic options with genetic programming

# Getting the data into a workable format

1) Open `data_formatting.py` and change date window, securities tickers, exercise date, etc. to be what you want. Note that DRD has not yet implemented the calculation of risk free interest rate so **don't change that.**

2) Run `data_formatting.py`

3) Run `data_2_matlab.sh`

4) Run `data_2_trader.py`. This will create four files: `ch.pkl`, `cl.pkl`, `ph.pkl`, `pl.pkl`, which respectively have all necessary data (except for risk free rate as discussed above) and option prices for "high" call, "low" call, "high" put, and "low" put. 

**UPDATE**: To run an arbitrary experiment of your chosing...
+ Open `experiment_N_runner.py` and modify parameters as necessary
+ Open `trader_sim.py` and add / modify / delete code as necessary
+ Run `python experiment_N_runner.py`, which will automatically perform all of the above data generation steps and then execute your experiment.

# Running Experiment N

1) Copy the `EXPERIMENT_N` folder to a new folder with `mkdir your_new_folder` and `cp -r EXPERIMENT_1/* your_new_folder` 

2) `cd your_new_folder`

3) Run `python experiment_N_runner.py`. It will automatically overwrite the results of the previous iteration of the experiment, so watch out for that. 

# FAQ

1) What's genetic programming?

Good question. To make a (very) long story short, genetic programming is a means by which *in silico* evolution is harnessed to enable computers to solve problems with minimal human involvement. Here, we apply genetic programming to the trading of options by giving the computer some basic operations it can perform, like buying or selling call or put options, and some variables it can reference, such as whether or not it's making money and whether or not an underlying security has risen or fallen in price. 

2) What's an exotic option?

Also a good question. An exotic option is a type of option that, in fine mathematical form, is better defined as what it is not. An exotic option is not a "vanilla" option, which means that it either has unconventional exercise dates, a more nuanced treatment of underlying assets, initially undefined characteristics, or some combination of the three. We're considering the case of rainbow options, which are options priced on the maximum or minimum of two risky assets. 

# More specific FAQ

1) Why don't you include "real-world" data for the risk-free interest rate when pricing your options? It's part of the pricing formula! 

This is true. However, it's well-known that options (on equities) are relatively insensitive to changes in the risk-free rate, especially when they're far from expiry. Since we only consider European options that are, at the closest, six months from expiry, we feel that including the risk-free rate would be adding another degree of freedom into our model and wouldn't contribute to the outcome of the evolution. 

2) On that subject, why do you only consider options so far from expiry? It seems a little unrealistic. 

It is and it isn't. It's entirely unrealistic on one hand, seeing as many market participants buy options solely to exercise them. However, it's also a perfectly fine assumption for two reasons. 
First, when you consider the trading landscape of long-dated European options, note that many market participants are buying options purely for speculative purposes with every intent to sell before the expiry date. Second, many of the strategies that the evolution comes up with (e.g., put-option hedging) we know to be valid, time-tested strategies even when expiry is explictly on the table. 

Practically speaking, implementing a market simulator that allows for the exercising of options is an order of magnitude more diffcult than the simulator we have working now. We'd like to do it at some point in the future, but it would take time that none of us currently have. 
